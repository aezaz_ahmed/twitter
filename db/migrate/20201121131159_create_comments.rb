class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.belongs_to :post, index: {:name => "index_in_comments_on_post_id"}
      t.belongs_to :user, index: {:name => "index_in_comments_on_user_id"}
      t.string :username
      t.string :content
      t.integer :likes_count, default: 0

      t.timestamps
    end
  end
end
