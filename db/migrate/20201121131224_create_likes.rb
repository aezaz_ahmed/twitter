class CreateLikes < ActiveRecord::Migration[6.0]
  def change
    create_table :likes do |t|
      t.belongs_to :user, index: {:name => "index_in_likes_on_user_id"}
      t.string :username
      t.references :likeable, polymorphic: true

      t.timestamps
    end
  end
end
