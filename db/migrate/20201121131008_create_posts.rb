class CreatePosts < ActiveRecord::Migration[6.0]
  def change
    create_table :posts do |t|
      t.belongs_to :user, index: {:name => "index_in_posts_on_user_id"}
      t.string :username
      t.string :tweet
      t.string :tweet_type
      t.string :status, default: 'draft'
      t.integer :comments_count, default: 0
      t.integer :likes_count, default: 0

      t.timestamps
    end
  end
end
