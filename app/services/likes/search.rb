# frozen_string_literal: true

module Likes
  class Search < ApplicationService #:nodoc:
    
    param :conditions
    option :limit, default: proc { 10 }
    option :page, default: proc { 0 }
    option :offset, default: proc { SearchHelper.offset(@page, @limit) }
    option :sort_column, default: proc { :created_at }
    option :sort_order, default: proc { :desc }
  
    def call
      search_likes
    end
  
    private
  
    def search_likes
      @likes = Like.where(@conditions)
                   .offset(@offset)
                   .limit(@limit)
      response
    end
  
    def response
      Success(message: 'Likes found', data: { likes: @likes,
                                              page: @page,
                                              limit: @limit,
                                              offset: @offset,
                                              total: Like.where(@conditions)
                                                         .count })
    end
  end
  
end
