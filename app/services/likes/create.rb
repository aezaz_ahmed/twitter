# frozen_string_literal: true

module Likes
  class Create < ApplicationService #:nodoc:
    param :user, (proc { |value| SearchHelper.find(User, value) })
    param :post, (proc { |value| SearchHelper.find(Post, value) })
    param :comment, (proc { |value| SearchHelper.find(Comment, value) })
  
    def call
      create
    end
  
    private
  
    def create
      @like = Like.new(create_params)
      if @like.save
        update_likes_count
        Success(message: 'Liked', data: @like)
      else
        Failure(message: @like.errors.full_messages.to_sentence)
      end
    end
  
    def create_params
      {
        user_id:  @user.id,
        username: @user.username,
        likeable: @post || @comment
      }
    end
  
    def update_likes_count
      @post.increment!(:likes_count) unless @post.nil?
      @comment.increment!(:likes_count) unless @comment.nil?
    end
  end    
end
