# frozen_string_literal: true

module Users
  class Update < ApplicationService #:nodoc:
    
    param :user, (proc { |value| SearchHelper.find(User, value) })
    param :update_params
  
    def call
      update
    end
  
    private
  
    def update
      if @user.update(@update_params)
        Success(message: 'User updated', data: @user)
      else
        Failure(message: @user.errors.full_messages.to_sentence)
      end
    end
  end  
end
