# frozen_string_literal: true

module Users
  class Create < ApplicationService #:nodoc:
  
    param :create_params
  
    def call
      create
    end
  
    private
  
    def create
      @user = User.new(@create_params)
      if @user.save
        Success(message: 'User created', data: @user)
      else
        Failure(message: @user.errors.full_messages.to_sentence)
      end
    end
  end  
end