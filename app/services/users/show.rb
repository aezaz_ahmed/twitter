# frozen_string_literal: true

module Users
  class Show < ApplicationService #:nodoc:

    param :user, (proc { |value| SearchHelper.find(User, value) })

    def call
      show
    end

    private

    def show
      # Needed just in case if we have to send some other data with it or have
      # to modify the response that can be done easily through services
      Success(message: 'user', data: @user)
    end
  end
end
