#frozen_string_literal: true

module Posts
  class MyPosts < ApplicationService #:nodoc:

    param :user, (proc { |value| SearchHelper.find(User, value) })
    param :conditions
    option :limit, default: proc { 10 }
    option :page, default: proc { 0 }
    option :offset, default: proc { SearchHelper.offset(@page, @limit) }
    option :sort_column, default: proc { :created_at }
    option :sort_order, default: proc { :desc }
  
    def call
      search
    end
  
    private
  
    def search
      @posts = Post.where(search_conditions)
                   .where.not(status: 'deleted')
                   .offset(@offset)
                   .order(@sort_column => @sort_order)
                   .limit(@limit)
      Success(message: 'Posts found', data: { posts:  @posts,
                                              page:   @page,
                                              limit:  @limit,
                                              offset: @offset,
                                              total:  Post.where(search_conditions)
                                                          .where.not(status: 'deleted')
                                                          .count })
    end
  
    def search_conditions
      (@conditions || {}).merge('user_id' => @user.id)
    end
  end
end
