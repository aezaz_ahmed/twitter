#frozen_string_literal: true

module Posts
  class Publish < ApplicationService #:nodoc:

    param :user, (proc { |value| SearchHelper.find(User, value) })
    param :post, (proc { |value| SearchHelper.find(Post, value) })
  
    def call
      return Failure(message: 'You are not allowed to publish this post') unless @user.id.eql?(@post.user_id)
  
      publish
    end
  
    private
  
    def publish
      if @post.update(status: 'published')
        Success(message: 'Post published', data: @post)
      else
        Failure(message: @post.errors.full_messages.to_sentence)
      end
    end
  end
end
