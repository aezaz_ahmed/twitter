#frozen_string_literal: true

module Posts
  class Create < ApplicationService #:nodoc:
    
    param :user, (proc { |value| SearchHelper.find(User, value) })
    param :create_params

    def call
      create
    end

    private

    def create
      @post = Post.new(create_params)
      if @post.save
        Success(message: 'Post created', data: @post)
      else
        Failure(message: @post.errors.full_messages.to_sentence)
      end
    end

    def create_params
      @create_params.merge(user_id:  @user.id, username: @user.username)
    end
  end
end
