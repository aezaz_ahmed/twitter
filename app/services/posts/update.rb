#frozen_string_literal: true

module Posts
  class Update < ApplicationService #:nodoc:

    param :user, (proc { |value| SearchHelper.find(User, value) })
    param :post, (proc { |value| SearchHelper.find(Post, value) })
    param :update_params
  
    def call
      return Failure(message: 'You are not allowed to edit this post') unless @user.id.eql?(@post.user_id)
      return Failure(message: 'Post cannot be updated once published') if @post.status.eql?('published')
  
      update
    end
  
    private
  
    def update
      if @post.update(@update_params)
        Success(message: 'Post updated', data: @post)
      else
        Failure(message: @post.errors.full_messages.to_sentence)
      end
    end
  end
end
