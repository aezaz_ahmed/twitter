# frozen_string_literal: true

module Posts
  class Show < ApplicationService #:nodoc:

    param :post, (proc { |value| SearchHelper.find(Post, value) })

    def call
      show
    end

    private

    def show
      # Needed just in case if we have to send some other data with it or have
      # to modify the response that can be done easily through services
      Success(message: 'post', data: @post)
    end
  end
end
