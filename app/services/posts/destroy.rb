#frozen_string_literal: true

module Posts
  class Destroy < ApplicationService #:nodoc:

    param :user, (proc { |value| SearchHelper.find(User, value) })
    param :post, (proc { |value| SearchHelper.find(Post, value) })
  
    def call
      return Failure(message: 'You are not allowed to delete this post') unless @user.id.eql?(@post.user_id)
  
      delete
    end
  
    private
  
    def delete
      if @post.update(status: 'deleted')
        Success(message: 'Post deleted', data: @post)
      else
        Failure(message: @post.errors.full_messages.to_sentence)
      end
    end
  end
end
