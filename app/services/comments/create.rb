# frozen_string_literal: true

module Comments
  class Create < ApplicationService #:nodoc:

    param :user, (proc { |value| SearchHelper.find(User, value) })
    param :post, (proc { |value| SearchHelper.find(Post, value) })
    param :create_params
  
    def call
      create_comment
    end
  
    private
  
    def create_comment
      @comment = Comment.new(create_params)
      if @comment.save
        increment_comment_count
        Success(message: 'Comment added', data: @comment)
      else
        Failure(message: @comment.errors.full_messages.to_sentence)
      end
    end
  
    def create_params
      @create_params.merge(user_id:  @user.id,
                           username: @user.username,
                           post_id:  @post.id)
    end
  
    def increment_comment_count
      @post.increment!(:comments_count)
    end
  end
end
