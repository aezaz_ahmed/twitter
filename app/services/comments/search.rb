# frozen_string_literal: true

module Comments
  class Search < ApplicationService #:nodoc:

    param :conditions
    option :limit, default: proc { 10 }
    option :page, default: proc { 0 }
    option :offset, default: proc { SearchHelper.offset(@page, @limit) }
    option :sort_column, default: proc { :created_at }
    option :sort_order, default: proc { :desc }
  
    def call
      search
    end
  
    private
  
    def search
      @comments = Comment.where(@conditions)
                         .offset(@offset)
                         .limit(@limit)
                         .order(@sort_column => @sort_order)
  
      Success(message: 'Comments found', data: { comments: @comments,
                                                 page:     @page,
                                                 limit:    @limit,
                                                 offset:   @offset,
                                                 total:    Comment.where(@conditions).count
                                                })
    end
  end  
end
