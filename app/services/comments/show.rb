# frozen_string_literal: true

module Comments
  class Show < ApplicationService #:nodoc:

    param :comment, (proc { |value| SearchHelper.find(Comment, value) })

    def call
      show
    end

    private

    def show
      # Needed just in case if we have to send some other data with it or have
      # to modify the response that can be done easily through services
      Success(message: 'Comment', data: @comment)
    end
  end
end
