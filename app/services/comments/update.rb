# frozen_string_literal: true

module Comments
  class Update < ApplicationService #:nodoc:

    param :user, (proc { |value| SearchHelper.find(User, value) })
    param :comment, (proc { |value| SearchHelper.find(Comment, value) })
    param :update_params

    def call
      return Failure(message: 'You are not allowed to edit this comment') unless @user.id.eql?(@comment.user_id)

      update
    end

    private

    def update
      if @comment.update(@update_params)
        Success(message: 'Comment updated', data: @comment)
      else
        Failure(message: @comment.errors.full_messages.to_sentence)
      end
    end
  end
end
