# frozen_string_literal: true

module SearchHelper #:nodoc:

  def self.find(model, object, options = {})
    attribute = options[:attribute].presence || :id
    return object if object.is_a?(model)
    model.find_by(attribute.to_sym => object)
  end
  
  def self.offset(page = 0, limit = 100)
    ([page.to_i, 1].max - 1) * limit.to_i
  end
end
