# frozen_string_literal: true

module ApiResponseHelper

  def generate_response
    @result.success? ? success_response : failure_response
  end

  def success_response(options = {})
    args = options.presence || @result.success
    response = {}
    response[:status] = args[:status] || 'success'
    response[:message] = args[:message] unless args[:message].blank?
    response[:data] = args[:data] unless args[:data].blank?
    render json: response, status: (args[:http_status] || :ok)
  end

  def failure_response(options = {})
    args = options.presence || @result.failure
    response = {}
    response[:status] = args[:status] || 'failure'
    response[:error] = args[:error] unless args[:error].blank?
    response[:error] = { message: args[:message] } if response[:error].blank?
    response[:data] = args[:data] unless args[:data].blank?
    render json: response, status: (args[:http_status] || :bad_request)
  end
end
