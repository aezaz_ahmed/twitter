# frozen_string_literal: true

class Like < ApplicationRecord #:nodoc:
  belongs_to :user
  belongs_to :likeable, polymorphic: true
end
