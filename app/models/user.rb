# frozen_string_literal: true

class User < ApplicationRecord #:nodoc:
  has_many :posts
  has_many :comments
  has_many :likes

  validates_presence_of :email, :username
  validates_uniqueness_of :email, :username
end
