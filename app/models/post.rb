# frozen_string_literal: true

class Post < ApplicationRecord #:nodoc:
  belongs_to :user
  has_many :comments
  has_many :likes, as: :likeable

  validates_presence_of :tweet, :tweet_type
end
