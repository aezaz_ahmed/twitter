# frozen_string_literal: true

class Comment < ApplicationRecord #:nodoc:
  belongs_to :user
  belongs_to :post
  has_many :likes, as: :likeable

  validates_presence_of :content
end
