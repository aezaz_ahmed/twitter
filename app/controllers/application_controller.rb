class ApplicationController < ActionController::Base
  include ApiResponseHelper
  skip_before_action :verify_authenticity_token

  private

  def set_user
    @user = User.find_by(username: params[:username] || params[:user_username])
    unless @user.present?
      failure_response(message: 'User not found')
    end
  end

  def set_post
    @post = Post.find_by(id: params[:post_id])
    unless @post.present?
      failure_response(message: 'Post not found')
    end
  end
end
