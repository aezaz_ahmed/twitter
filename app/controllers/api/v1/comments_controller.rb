# frozen_string_literal: true

module Api
  module V1
    class CommentsController < ApplicationController #:nodoc:
      before_action :set_user, only: %i[create show update]
      before_action :set_post, only: %i[create show update]
      before_action :set_comment, only: %i[show update]

      def create
        @result = Comments::Create.call(@user, @post, create_params)
        generate_response
      end

      def update
        @result = Comments::Update.call(@user, @comment, update_params)
        generate_response
      end

      def show
        @result = Comments::Show.call(@comment)
        generate_response
      end

      def search
        @result = Comments::Search.call(search_conditions, search_params)
        generate_response
      end

      private

      def set_comment
        @comment = Comment.find_by(id: params[:id])
        unless @comment.present?
          failure_response(message: 'Comment not found')
        end
      end

      def create_params
        params.require(:comment).permit(:content)
      end

      def update_params
        params.require(:comment).permit(:content)
      end

      def search_conditions
        search_params.delete(:conditions)
      end

      def search_params
        params.permit(:page,
                      :limit,
                      :offset,
                      :sort_order,
                      :sort_column,
                      conditions: {}).to_h.symbolize_keys
      end
    end
  end
end
