# frozen_string_literal: true

module Api
  module V1
    class LikesController < ApplicationController
      before_action :set_user, only: %i[create]
      before_action :find_likeable, only: %i[create]

      def create
        @result = Likes::Create.call(@user, @post, @comment, create_params)
        generate_response
      end

      def search
        @result = Likes::Search.call(search_conditions, search_params)
        generate_response
      end

      private

      def set_like
        @like = Like.find_by(id: params[:id])
        unless @like.present?
          failure_response(message: 'Like not found')
        end
      end

      def create_params
        params.require(:like).permit(:post_id, :comment_id)
      end

      def search_conditions
        search_params.delete(:conditions)
      end

      def search_params
        params.permit(:page, :limit, :offset, :sort_order, :sort_column, conditions: {}).to_h.symbolize_keys
      end

      def find_likeable
        if create_params['post_id'].present?
          @post = Post.find_by(id: create_params['post_id'])
          failure_response(message: 'Post not found') if @post.nil?
        elsif create_params['comment_id'].present?
          @comment = Comment.find_by(id: create_params['comment_id'])
          failure_response(message: 'Comment not found') if @comment.nil?
        end
      end
    end
  end
end
