# frozen_string_literal: true

module Api
  module V1
    class UsersController < ApplicationController #:nodoc:
      before_action :set_user, only: %i[update show]

      def create
        @result = Users::Create.call(create_params)
        generate_response
      end

      def update
        @result = Users::Update.call(@user, update_params)
        generate_response
      end

      def show
        @result = Users::Show.call(@user)
        generate_response
      end

      private

      def set_user
        @user = User.find_by(username: params[:username])
        unless @user.present?
          failure_response(message: 'User not found')
        end
      end

      def create_params
        params.require(:user).permit(:email, :username, :name)
      end

      def update_params
        params.require(:user).permit(:name)
      end
    end
  end
end
