# frozen_string_literal: true

module Api
  module V1
    class PostsController < ApplicationController #:nodoc:
      before_action :set_user, only: %i[create update publish destroy my_posts]
      before_action :set_post, only: %i[update show publish destroy]

      def create
        @result = Posts::Create.call(@user, create_params)  
        generate_response
      end

      def update
        @result = Posts::Update.call(@user, @post, update_params)
        generate_response
      end

      def show
        @result = Posts::Show.call(@post)
        generate_response
      end

      def publish
        @result = Posts::Publish.call(@user, @post)
        generate_response
      end

      def destroy
        @result = Posts::Destroy.call(@user, @post)
        generate_response
      end

      def my_posts
        @result = Posts::MyPosts.call(@user, search_conditions, search_params)
        generate_response
      end

      def search
        @result = Posts::Search.call(search_conditions, search_params)
        generate_response
      end

      private

      def set_post
        @post = Post.find_by(id: params[:id])
        unless @post.present?
          failure_response(message: 'Post not found')
        end
      end

      def create_params
        params.require(:post).permit(:tweet, :tweet_type)
      end

      def update_params
        params.require(:post).permit(:tweet, :tweet_type)
      end

      def search_conditions
        search_params.delete(:conditions)
      end

      def search_params
        params.permit(:page, :limit, :offset, :sort_order, :sort_column, conditions: {}).to_h.symbolize_keys
      end
    end
  end
end
