Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :users, param: :username, only: %i[create update show] do

        resources :likes, only: %i[create]

        resources :posts, only: %i[create update destroy] do
          member do
            patch 'publish'
          end
          collection do
            post 'my_posts'
          end

          resources :comments, only: %i[create update]
        end
      end
      
      resources :posts, only: %i[show] do
        collection do
          post 'search'
        end
      end

      resources :likes, only: [] do
        collection do
          post 'search'
        end
      end

      resources :comments, only: [] do
        collection do
          post 'search'
        end
      end
    end
  end
end
